export class PersonalData {
    fname: string = ''
    lname: string = ''
    gender: string = ''
    email: string = ''
    password: string = ''
    state: string = ''
    date: string =''
    exp:string=''
}