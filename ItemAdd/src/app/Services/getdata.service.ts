import { Injectable } from '@angular/core';
import { PersonalData } from '../Model/dataModel';

@Injectable({
  providedIn: 'root'
})
export class GetdataService {
  signList: PersonalData []= [];

  constructor() { }

  add(data) {
    console.log("data", data);
    this.signList.push(data);

  }

  showList() {
    return this.signList;
  }
}
