import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddItemComponent } from './Components/add-item/add-item.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataBindingComponent } from './Components/data-binding/data-binding.component';
import { RouterModule, Routes } from '@angular/router';

import { SignUpformComponent } from './Components/sign-upform/sign-upform.component';
import { SuccessSignupComponent } from './Components/success-signup/success-signup.component';
import { HeadercompComponent } from './Components/header/headercomp/headercomp.component';

const appRoutes: Routes = [
  { path: 'signup', component: SignUpformComponent },
  { path: 'successsign', component: SuccessSignupComponent  },
]; 
@NgModule({
  declarations: [
    AppComponent,
    AddItemComponent,
    DataBindingComponent,
    SignUpformComponent,
    SuccessSignupComponent,
    HeadercompComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
